package ru.penf00k.wiki_flutter_go

import io.flutter.app.FlutterApplication
import wrapper.Wrapper

class MainApplication : FlutterApplication() {

    override fun onCreate() {
        super.onCreate()
        Wrapper.initialize("https://en.wikipedia.org/w/api.php")
    }
}
