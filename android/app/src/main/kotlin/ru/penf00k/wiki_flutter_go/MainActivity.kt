package ru.penf00k.wiki_flutter_go

import android.os.Bundle
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import ru.pikremont.clientpikremont.flutter.getWandMethodCallHandler
import wrapper.Event
import wrapper.Wrapper

class MainActivity : FlutterActivity(), Event, EventChannel.StreamHandler {

    companion object {
        private const val CHANNEL = "ru.penf00k.wiki_flutter_go.CHANNEL"
        private const val EVENT_CHANNEL = "ru.penf00k.wiki_flutter_go.EVENT_CHANNEL"
    }

    private lateinit var channel: MethodChannel
    private lateinit var eventChannel: EventChannel
    private var eventSink: EventChannel.EventSink? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        channel = MethodChannel(flutterView, CHANNEL)
        channel.setMethodCallHandler(getWandMethodCallHandler())

        eventChannel = EventChannel(flutterView, EVENT_CHANNEL)
        eventChannel.setStreamHandler(this)

        Wrapper.registerEventCallback(this)
    }

    override fun onEvent(fullSubscriptionName: String?, bytes: ByteArray?) {
        this.runOnUiThread {
            channel.invokeMethod("subscriptionEvent", mapOf<String, Any?>(
                    "fullSubscriptionName" to fullSubscriptionName,
                    "payload" to bytes
            ))
        }
    }

    override fun onListen(arguments: Any?, eventSink: EventChannel.EventSink?) {
        this.eventSink = eventSink
    }

    override fun onCancel(arguments: Any?) {
        eventSink = null
    }
}
