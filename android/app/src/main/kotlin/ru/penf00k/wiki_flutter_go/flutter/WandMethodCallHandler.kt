package ru.pikremont.clientpikremont.flutter

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import wrapper.FuncCallback
import wrapper.Wrapper

fun getWandMethodCallHandler() = MethodChannel.MethodCallHandler { call, result ->
    when (call.method) {
        "callMethod" -> processCallMethod(call, result)
        "subscribe" -> processSubscribe(call, result)
        "unsubscribe" -> processUnsubscribe(call, result)
        else -> result.notImplemented()
    }


}

private fun processCallMethod(call: MethodCall, result: MethodChannel.Result) {
    val methodName = call.argument<String>("methodName")
    if (methodName == null)
        result.error(methodName, "you must provide methodName", null)

    val args = call.argument<ByteArray>("args")

    if (args != null) {
        Wrapper.callMethod(methodName, args, object : FuncCallback {
            override fun onSuccess(bytes: ByteArray?) {
                result.success(bytes)
            }

            override fun onError(err: String?) {
                result.error(methodName, err, null)
            }
        })
    } else {
        result.error(methodName, "args are empty byte array", null)
    }
}

private fun processSubscribe(call: MethodCall, result: MethodChannel.Result) {
    val subscriptionName = call.argument<String>("subscriptionName")
    if (subscriptionName == null)
        result.error(subscriptionName, "you must provide subscriptionName", null)

    val args = call.argument<ByteArray>("args")

    if (args != null) {
        Wrapper.subscribe(subscriptionName, args, object : FuncCallback {
            override fun onSuccess(bytes: ByteArray?) {
                bytes?.let { result.success(it) }
            }

            override fun onError(err: String?) {
                err?.let {
                    result.error(subscriptionName, it, null)
                }
            }
        })
    } else {
        result.error(subscriptionName, "args are empty byte array", null)
    }
}

private fun processUnsubscribe(call: MethodCall, result: MethodChannel.Result) {
    val fullSubscriptionName = call.argument<String>("fullSubscriptionName")
    if (fullSubscriptionName == null)
        result.error(fullSubscriptionName, "you must provide fullSubscriptionName", null)

    Wrapper.unsubscribe(fullSubscriptionName)
}
