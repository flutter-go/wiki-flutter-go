fba: # build release apk
	flutter build apk

ir: # install built release apk
	adb -d install -r ./build/app/outputs/apk/release/app-release.apk

fi: # build and install release apk
	flutter build apk