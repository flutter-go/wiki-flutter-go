package di

import (
	log "github.com/sirupsen/logrus"
	"go.uber.org/dig"
	"net/http"
	"wiki_yandex/gateway/api"
)

func BuildContainer(config api.Config) (*dig.Container, error) {
	log.Tracef("Building di container")

	container := dig.New()

	if err := container.Provide(func() *api.Config {
		return &config
	}); err != nil {
		log.Errorf("Failed to provide [api.Config] with error %v", err)
		return nil, err
	}

	if err := container.Provide(func() *api.Client {
		return &api.Client{
			HttpClient: &http.Client{},
			Config:     config,
		}
	}); err != nil {
		log.Errorf("Failed to provide [api.Client] with error %v", err)
		return nil, err
	}

	if err := provideGateways(container); err != nil {
		return nil, err
	}

	if err := provideInteractors(container); err != nil {
		return nil, err
	}

	return container, nil
}
