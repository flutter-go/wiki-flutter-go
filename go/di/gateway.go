package di

import (
	log "github.com/sirupsen/logrus"
	"go.uber.org/dig"
	"wiki_yandex/gateway/api/wiki"
)

func provideGateways(container *dig.Container) error {
	if err := container.Provide(wiki.New); err != nil {
		log.Errorf("Failed to provide [gateway.wiki] with error %v", err)
		return err
	}

	return nil
}
