package di

import (
	log "github.com/sirupsen/logrus"
	"go.uber.org/dig"
	"wiki_yandex/interactor/article"
)

func provideInteractors(container *dig.Container) error {
	if err := container.Provide(article.New); err != nil {
		log.Errorf("Failed to provide [interactor.wiki] with error %v", err)
		return err
	}

	return nil
}
