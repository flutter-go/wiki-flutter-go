package wiki_yandex

import "wiki_yandex/model"

type Article struct {
	PageID int
	Title  string
	Images []Image
}

func toArticleDto(data model.Article) Article {
	return Article{
		PageID: data.PageID,
		Title:  data.Title,
		Images: toImageDtoList(data.Images),
	}
}

func toArticleDtoList(data []model.Article) []Article {
	if data == nil {
		return nil
	}

	res := make([]Article, len(data))
	for i, v := range data {
		res[i] = toArticleDto(v)
	}

	return res

}

type Image struct {
	Title string
	Url   string
}

func toImageDto(data model.Image) Image {
	return Image{
		Title: data.Title,
		Url:   data.Url,
	}
}

func toImageDtoList(data []model.Image) []Image {
	if data == nil {
		return nil
	}

	res := make([]Image, len(data))
	for i, v := range data {
		res[i] = toImageDto(v)
	}

	return res
}
