package gateway

import "wiki_yandex/model"

type Wiki interface {
	GetAllArticles(latLng model.LatLng) ([]model.Article, error)
	GetImagesForArticle(pageID int) ([]model.Image, error)
}
