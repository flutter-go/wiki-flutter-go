package wiki

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"net/http"
	"wiki_yandex/gateway"
	"wiki_yandex/gateway/api"
	"wiki_yandex/gateway/api/dto"
	"wiki_yandex/model"
)

type Article struct {
	client *api.Client
}

func New(client *api.Client) (gateway.Wiki, error) {
	return &Article{
		client: client,
	}, nil
}

func (a *Article) GetAllArticles(latLng model.LatLng) ([]model.Article, error) {
	req, err := http.NewRequest("GET", "https://en.wikipedia.org/w/api.php", nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to make NewRequest for GetAllArticles from gateway.Wiki")
	}
	q := req.URL.Query()
	q.Add("action", "query")
	q.Add("format", "json")
	q.Add("list", "geosearch")
	q.Add("gscoord", latLng.String())
	q.Add("gsradius", "5000")
	q.Add("gslimit", "5")

	req.URL.RawQuery = q.Encode()

	resp, err := a.client.HttpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to GetAllArticles from gateway.Wiki")
	}
	defer resp.Body.Close()

	var res dto.WikiGeoSearchResult
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return nil, errors.Wrap(err, "failed to Decode for GetAllArticles from gateway.Wiki")
	}

	result := make([]model.Article, len(res.Query.GeoSearch))
	for i, v := range res.Query.GeoSearch {
		result[i] = model.Article{
			PageID: v.PageID,
			Title:  v.Title,
		}
	}

	return result, nil
}

func (a *Article) GetImagesForArticle(pageID int) ([]model.Image, error) {
	host := a.client.Config.Host
	req, err := http.NewRequest("GET", fmt.Sprintf("%v/w/api.php", host), nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to make NewRequest for GetAllArticles from gateway.Wiki")
	}
	q := req.URL.Query()
	q.Add("action", "parse")
	q.Add("format", "json")
	q.Add("prop", "images")
	q.Add("pageid", fmt.Sprintf("%v", pageID))

	req.URL.RawQuery = q.Encode()

	resp, err := a.client.HttpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to GetImagesForArticle from gateway.Wiki")
	}
	defer resp.Body.Close()

	var res dto.WikiImagesParseResult
	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return nil, errors.Wrap(err, "failed to Decode for GetImagesForArticle from gateway.Wiki")
	}

	result := make([]model.Image, len(res.Parse.Images))
	for i, v := range res.Parse.Images {
		result[i] = model.Image{
			Title: v,
		}
	}

	return result, nil
}
