package wiki

import (
	"fmt"
	"net/http"
	"testing"
	"wiki_yandex/gateway/api"
	"wiki_yandex/model"
)

func TestWiki_GetAllArticles(t *testing.T) {
	type fields struct {
		client *api.Client
	}
	type args struct {
		latLng model.LatLng
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Article
		wantErr bool
	}{
		{
			name: "test 1",
			fields: fields{
				client: &api.Client{
					HttpClient: &http.Client{},
					Config: api.Config{
						Host: "https://en.wikipedia.org",
					},
				},
			},
			args: args{
				latLng: model.LatLng{
					Lat: 55.734295,
					Lng: 37.613536,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Article{
				client: tt.fields.client,
			}
			got, err := a.GetAllArticles(tt.args.latLng)
			if (err != nil) != tt.wantErr {
				t.Errorf("Wiki.GetAllArticles() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			fmt.Printf("GetAllArticles res: %+v", got)
		})
	}
}

func TestWiki_GetImagesForArticle(t *testing.T) {
	type fields struct {
		client *api.Client
	}
	type args struct {
		pageID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Image
		wantErr bool
	}{
		{
			name: "test 1",
			fields: fields{
				client: &api.Client{
					HttpClient: &http.Client{},
					Config: api.Config{
						Host: "https://en.wikipedia.org",
					},
				},
			},
			args: args{
				pageID: 53056,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &Article{
				client: tt.fields.client,
			}
			got, err := a.GetImagesForArticle(tt.args.pageID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Wiki.GetImagesForArticle() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			fmt.Printf("GetImagesForArticle res: %+v", got)
		})
	}
}
