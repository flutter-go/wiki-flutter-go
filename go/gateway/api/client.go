package api

import "net/http"

type Config struct {
	Host string
}

type Client struct {
	HttpClient *http.Client
	Config     Config
}

func NewClient(config Config) (*Client, error) {
	return &Client{
		HttpClient: &http.Client{},
		Config:     config,
	}, nil
}
