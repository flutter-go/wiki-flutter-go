package dto

type WikiGeoSearchResult struct {
	BatchComplete string         `json:"batchcomplete"`
	Query         GeoSearchQuery `json:"query"`
}

type GeoSearch struct {
	PageID  int     `json:"pageid"`
	Ns      int     `json:"ns"`
	Title   string  `json:"title"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
	Dist    float64 `json:"dist"`
	Primary string  `json:"primary"`
}

type GeoSearchQuery struct {
	GeoSearch []GeoSearch `json:"geosearch"`
}
