package dto

//type WikiImagesResult struct {
//	BatchComplete string     `json:"batchcomplete"`
//	Query         ImageQuery `json:"query"`
//	Limits        Limits     `json:"limits"`
//}
//
//type Normalized struct {
//	From string `json:"from"`
//	To   string `json:"to"`
//}
//
//type Images struct {
//	Ns    int    `json:"ns"`
//	Title string `json:"title"`
//}
//
//type Page struct {
//	PageID int      `json:"pageid"`
//	Ns     int      `json:"ns"`
//	Title  string   `json:"title"`
//	Images []Images `json:"images"`
//}
//
//type ImageQuery struct {
//	Normalized []Normalized    `json:"normalized"`
//	Pages      map[string]Page `json:"pages"`
//}
//
//type Limits struct {
//	Images int `json:"images"`
//}

//////////////////////////////////////////////////////////////////////
type WikiImagesParseResult struct {
	Parse ImageParse `json:"parse"`
}

//func (r WikiImagesParseResult) ToModel() model.Image {
//	return model.Image{
//		Title: r.Parse.Title,
//	}
//}

type ImageParse struct {
	Title  string   `json:"title"`
	PageID int      `json:"pageid"`
	Images []string `json:"images"`
}
