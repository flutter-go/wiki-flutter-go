package article

import (
	"wiki_yandex/gateway"
	"wiki_yandex/interactor"
	"wiki_yandex/model"
)

type Interactor struct {
	api gateway.Wiki
}

func New(api gateway.Wiki) (interactor.Wiki, error) {
	return &Interactor{
		api: api,
	}, nil
}

func (i *Interactor) GetAllArticles(latLng model.LatLng) ([]model.Article, error) {
	arts, err := i.api.GetAllArticles(latLng)
	if err != nil {
		return nil, err
	}

	for k, v := range arts {
		images, err := i.api.GetImagesForArticle(v.PageID)
		if err != nil {
			return nil, err
		}

		arts[k].Images = images
	}

	return arts, nil
}
