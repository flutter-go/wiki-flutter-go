package main

import (
	log "github.com/sirupsen/logrus"
	"wiki_yandex"
)

func main() {
	res, err := wiki_yandex.GetAllArticles(55.734295, 37.613536)
	if err != nil {
		panic(err)
	}
	log.Printf("res: %+v", res)
}
