module wiki_yandex

go 1.12

require (
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/dig v1.7.0
)
