package model

import "fmt"

type Article struct {
	PageID int
	Title  string
	Images []Image
}

type Image struct {
	Title string
	Url   string
}

type LatLng struct {
	Lat float32
	Lng float32
}

func (ll LatLng) String() string {
	return fmt.Sprintf("%.7f|%.7f", ll.Lat, ll.Lng)
}
