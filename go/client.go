package wiki_yandex

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"go.uber.org/dig"
	"wiki_yandex/di"
	"wiki_yandex/gateway/api"
)

// @ignore:
type Client struct {
	container *dig.Container
}

var cl *Client

func init() {
	Initialize("https://en.wikipedia.org/w/api.php")
}

func Initialize(host string) {
	log.SetLevel(log.TraceLevel)

	if cl == nil {
		log.Tracef("Creating new client")
		log.Tracef("Starting initialization, host: %v", host)

		config := api.Config{
			Host: host,
		}

		container, err := di.BuildContainer(config)
		if err != nil {
			panic(fmt.Sprintf("failed to build container: %v", err))
		}
		log.Tracef("Container built successfully")

		cl = &Client{container: container}
	}
}
