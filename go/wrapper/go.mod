module wrapper

go 1.12

require (
	github.com/PenF00k/go-wand v0.0.0-20190615111610-2d0ee8f74a16
	github.com/golang/protobuf v1.3.1
	github.com/mitchellh/mapstructure v1.1.2
	gitlab.vmassive.ru/go/wand v2.0.0+incompatible // indirect
	wiki_yandex v0.0.0
)

replace wiki_yandex => ../
