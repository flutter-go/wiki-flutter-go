package wrapper

import (
	"github.com/PenF00k/go-wand/goapi"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/mitchellh/mapstructure"
	"strconv"
	"wiki_yandex"
	"wrapper/proto_client"
)

type FuncCallback interface {
	OnSuccess(bytes []byte)
	OnError(err string)
}

type Event interface {
	OnEvent(eventName string, bytes []byte)
}

type EventCallback interface {
	OnEvent(bytes []byte)
}

type Subscription interface {
	Cancel()
}

type eventWrapper struct {
	event Event
}

func (eventer eventWrapper) OnEvent(eventName string, bytes []byte) {
	eventer.event.OnEvent(eventName, bytes)
}

func newEventWrapper(event Event) goapi.Event {
	return &eventWrapper{
		event: event,
	}
}

type funcCallbackWrapper struct {
	callback FuncCallback
}

func (caller funcCallbackWrapper) OnSuccess(bytes []byte) {
	caller.callback.OnSuccess(bytes)
}

func (caller funcCallbackWrapper) OnError(err string) {
	caller.callback.OnError(err)
}

func newFuncCallbackWrapper(callback FuncCallback) goapi.FuncCallback {
	return &funcCallbackWrapper{
		callback: callback,
	}
}

// Registry for all calls
var registry = goapi.NewRegistry()

type X_____xxxx struct{ Val string }

func Ping____(number int) string {
	return strconv.Itoa(number)
}

func Ping____XXX(val interface{}) string {
	data := X_____xxxx{}
	mapstructure.Decode(val, &val)
	return data.Val
} // CallMethod - call from client
func CallMethod(methodName string, args []byte, callback FuncCallback) {
	wrapper := newFuncCallbackWrapper(callback)
	registry.Call(methodName, args, wrapper)
}

func RegisterEventCallback(onEvent Event) {
	wrapper := newEventWrapper(onEvent)
	registry.RegisterEventCallback(wrapper)
}

func RemoveEventCallback() {
	registry.RegisterEventCallback(nil)
}

// Subscribe - Subscribe from client
func Subscribe(subscriptionName string, args []byte, callback FuncCallback) {
	fullSubName, err := registry.Subscribe(subscriptionName, args)
	if err == nil {
		w := &wrappers.StringValue{
			Value: fullSubName,
		}
		bytes, err := proto.Marshal(w)
		if err == nil {
			callback.OnSuccess(bytes)
			return
		}
	}

	callback.OnError(err.Error())
}

// Unsubscribe - Unsubscribe from client
func Unsubscribe(subscriptionName string) {
	registry.CancelSubscription(subscriptionName)
}

func init() {

	registry.RegisterFunction("JustForFun", callAdapterForJustForFun)
	registry.RegisterFunction("GetAllArticles", callAdapterForGetAllArticles)

}

// function is pure
func Initialize(host string) {
	wiki_yandex.Initialize(host)
}

func callAdapterForJustForFun(argsBytes []byte, callback goapi.FuncCallback) {
	args := proto_client.JustForFunArgs{}

	if err := proto.Unmarshal(argsBytes, &args); err != nil {
		callback.OnError(err.Error())
		return
	}

	//0

	genArgString := func(data string) string {
		return string(data)
	}

	res, err := wiki_yandex.JustForFun(genArgString(args.Text))
	if err != nil {
		callback.OnError(err.Error())
	} else {
		//0

		genString := func(data string) string {
			return string(data)
		}

		//1
		genLovelyObject := func(data wiki_yandex.LovelyObject) *proto_client.LovelyObject {
			res := &proto_client.LovelyObject{
				TextInsideYa: genString(data.TextInsideYa),
			}

			return res
		}

		//2
		genLovelyObjectPointer := func(data *wiki_yandex.LovelyObject) *proto_client.LovelyObject {
			if data == nil {
				return nil
			}

			res := genLovelyObject(*data)
			return res
		}

		bytes, err := proto.Marshal(genLovelyObjectPointer(res))
		if err == nil {
			callback.OnSuccess(bytes)
		} else {
			callback.OnError(err.Error())
		}
	}
}

func callAdapterForGetAllArticles(argsBytes []byte, callback goapi.FuncCallback) {
	args := proto_client.GetAllArticlesArgs{}

	if err := proto.Unmarshal(argsBytes, &args); err != nil {
		callback.OnError(err.Error())
		return
	}

	//0

	genArgFloat32 := func(data float32) float32 {
		return float32(data)
	}

	res, err := wiki_yandex.GetAllArticles(genArgFloat32(args.Lat), genArgFloat32(args.Lng))
	if err != nil {
		callback.OnError(err.Error())
	} else {
		//0

		genInt := func(data int) int32 {
			return int32(data)
		}

		//1

		genString := func(data string) string {
			return string(data)
		}

		//2
		genImage := func(data wiki_yandex.Image) *proto_client.Image {
			res := &proto_client.Image{
				Title: genString(data.Title),
				Url:   genString(data.Url),
			}

			return res
		}

		//3
		genImageSlice := func(data []wiki_yandex.Image) []*proto_client.Image {
			res := make([]*proto_client.Image, 0, len(data))

			for _, v := range data {
				d := genImage(v)
				res = append(res, d)
			}

			return res
		}

		//4
		genArticle := func(data wiki_yandex.Article) *proto_client.Article {
			res := &proto_client.Article{
				PageID: genInt(data.PageID),
				Title:  genString(data.Title),
				Images: genImageSlice(data.Images),
			}

			return res
		}

		//5
		genArticleSlice := func(data []wiki_yandex.Article) []*proto_client.Article {
			res := make([]*proto_client.Article, 0, len(data))

			for _, v := range data {
				d := genArticle(v)
				res = append(res, d)
			}

			return res
		}

		//6
		genArticleList := func(data wiki_yandex.ArticleList) *proto_client.ArticleList {
			res := &proto_client.ArticleList{
				List: genArticleSlice(data.List),
			}

			return res
		}

		//7
		genArticleListPointer := func(data *wiki_yandex.ArticleList) *proto_client.ArticleList {
			if data == nil {
				return nil
			}

			res := genArticleList(*data)
			return res
		}

		bytes, err := proto.Marshal(genArticleListPointer(res))
		if err == nil {
			callback.OnSuccess(bytes)
		} else {
			callback.OnError(err.Error())
		}
	}
}
