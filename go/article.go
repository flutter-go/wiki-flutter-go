package wiki_yandex

import (
	"fmt"
	"wiki_yandex/interactor"
	"wiki_yandex/model"
)

type LovelyObject struct {
	TextInsideYa string
}

var counter = 0

func JustForFun(text string) (*LovelyObject, error) {
	counter++

	return &LovelyObject{
		TextInsideYa: fmt.Sprintf("%v: %v", text, counter),
	}, nil
}

type ArticleList struct {
	List []Article
}

func GetAllArticles(lat float32, lng float32) (*ArticleList, error) {
	var res *ArticleList

	err := cl.container.Invoke(func(interactor interactor.Wiki) error {
		tmp, err := interactor.GetAllArticles(model.LatLng{
			Lat: lat,
			Lng: lng,
		})

		res = &ArticleList{
			List: toArticleDtoList(tmp),
		}
		return err
	})

	return res, err
}
