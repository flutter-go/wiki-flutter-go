{{- $Args := .Function.Args -}}
{{- $ReturnValues := .Function.ReturnValues -}}
{{- $ProtoPackageName := .CodeList.PackageMap.ProtoPackageName -}}
{{- define "gen_field" }}
    {{ if .IsPrimitive -}}
        {{- template "gen_primitive" . -}}
    {{- else if .IsPrimitivePointer -}}
        {{- template "gen_wrapped_primitive" . -}}
    {{- else if .Selector -}}
        {{- template "gen_selector" . -}}
    {{- else if .Struct -}}
        {{- template "gen_struct" . -}}
    {{- else if .Slice -}}
        {{- template "gen_slice" . -}}
    {{- else if .Pointer -}}
        {{- template "gen_pointer" . -}}
    {{- end -}}
{{ end -}}

{{- define "gen_primitive" }}
    gen{{ .GetGenFuncName }} := func(data {{ .Name }}) {{ .GetWrapperName (toBoolPointer true) false }} {
        return {{ .WrapToProtoType "data" }}
    }
{{ end -}}

{{- define "gen_wrapped_primitive" }}
    gen{{ .GetGenFuncName }} := func(data {{ .GetParamTypeName "" false }}) {{ .GetWrapperName (toBoolPointer true) false }} {
        if data == nil {
            return nil
        }

        return {{ .GetWrapperName (toBoolPointer false) false }}{
            Value: {{ .WrapToProtoType "*data" }},
        }
    }
{{ end -}}

{{- define "gen_selector" }}
    {{/*{{ if and eq .Selector.Package "time" eq .Selector.TypeName "Time" }}*/}}
    {{ if eq .Selector.Package "time" }}
        {{- template "gen_time" . -}}
    {{ end }}
{{ end -}}

{{- define "gen_time" }}
    genTimeTime := func(data time.Time) *timestamp.Timestamp {
        return &timestamp.Timestamp{
            Seconds: data.Unix(),
            Nanos:   int32(data.Nanosecond()),
        }
    }
{{ end -}}

{{- define "gen_struct" }}
    {{- $UpperCaseName := .Name.ToUpperCamelCase -}}
    {{- $Package := "wiki_yandex" -}}
    {{ $ProtoPackageName := "proto_client" -}}

    gen{{ $UpperCaseName }} := func(data {{ .GetParamTypeName $Package false }}) {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }} {
        res := {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer false) false }}{
        {{ range $item := .Struct.Fields }}
            {{- if $item.IsExported -}}
            {{ $item.GetUpperCamelCaseName "" "" }}: gen{{ .Type.GetGenFuncName }}(data.{{ $item.Name }}),
            {{ end -}}
        {{ end }}
        }

        return res
    }
{{ end -}}

{{- define "gen_slice" }}
    {{- $Package := "wiki_yandex" -}}
    {{- $ProtoPackageName := "proto_client" -}}

    gen{{ .GetGenFuncName }} := func(data {{ .GetParamTypeName $Package false }}) {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }} {
        res := make({{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }}, 0, len(data))

        for _, v := range data {
            {{ if .Slice.InnerType.Pointer -}}
            if v == nil {
                res = append(res, nil)
                continue
            }
            {{ end -}}
            d := gen{{ .Slice.InnerType.GetGenFuncName }}(v)
            res = append(res, d)
        }

        return res
    }
{{ end -}}

{{- define "gen_pointer" }}
    {{- $Package := "wiki_yandex" -}}
    {{- $ProtoPackageName := "proto_client" -}}

    gen{{ .GetGenFuncName }} := func(data {{ .GetParamTypeName $Package false }}) {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }} {
        if data == nil {
            return nil
        }

        res := gen{{ .Pointer.InnerType.GetGenFuncName }}(*data)
        return res
    }
{{ end -}}



{{- define "gen_field_reverse" }}
    {{ if .IsPrimitive -}}
        {{- template "gen_primitive_reverse" . -}}
    {{- else if .IsPrimitivePointer -}}
        {{- template "gen_wrapped_primitive_reverse" . -}}
    {{- else if .Selector -}}
        {{- template "gen_selector_reverse" . -}}
    {{- else if .Struct -}}
        {{- template "gen_struct_reverse" . -}}
    {{- else if .Slice -}}
        {{- template "gen_slice_reverse" . -}}
    {{- else if .Pointer -}}
        {{- template "gen_pointer_reverse" . -}}
    {{- end -}}
{{ end -}}

{{- define "gen_primitive_reverse" }}
    genArg{{ .GetGenFuncName }} := func(data {{ .GetWrapperName (toBoolPointer false) true }}) {{ .Name }} {
        return {{ .WrapToDefaultGoType "data" }}
    }
{{ end -}}

{{- define "gen_wrapped_primitive_reverse" }}
    {{- $Package := "wiki_yandex" -}}

    genArg{{ .GetGenFuncName }} := func(data {{ .GetWrapperName (toBoolPointer true) true }}) {{ .GetReturnTypeName $Package (toBoolPointer true) true }} {
        if data == nil {
            return nil
        }

        res := data.Value
        return &res
    }
{{ end -}}

{{- define "gen_selector_reverse" }}
    {{/*{{ if and eq .Selector.Package "time" eq .Selector.TypeName "Time" }}*/}}
    {{ if eq .Selector.Package "time" }}
        {{- template "gen_time_reverse" . -}}
    {{ end }}
{{ end -}}

{{- define "gen_time_reverse" }}
    genArgTimeTime := func(data *timestamp.Timestamp) time.Time {
        t, err := ptypes.Timestamp(data)
        if err != nil {
            callback.OnError(err.Error())
        }
        return t
    }
{{ end -}}

{{- define "gen_struct_reverse" }}
    {{- $UpperCaseName := .Name.ToUpperCamelCase -}}
    {{- $Package := "wiki_yandex" -}}
    {{ $ProtoPackageName := "proto_client" -}}

    genArg{{ $UpperCaseName }} := func(data {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }}) {{ .GetReturnTypeName $Package nil false }} {
    res := {{ .GetReturnTypeName $Package nil false }}{
    {{ range $item := .Struct.Fields }}
        {{- if $item.IsExported -}}
            {{ $item.GetUpperCamelCaseName "" "" }}: genArg{{ .Type.GetGenFuncName }}(data.{{ $item.Name }}),
        {{ end -}}
    {{ end }}
    }

    return res
    }
{{ end -}}

{{- define "gen_slice_reverse" }}
    {{- $Package := "wiki_yandex" -}}
    {{- $ProtoPackageName := "proto_client" -}}

    genArg{{ .GetGenFuncName }} := func(data {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }}) {{ .GetReturnTypeName $Package nil true }} {
    res := make({{ .GetReturnTypeName $Package nil true }}, 0, len(data))

    for _, v := range data {
    {{ if .Slice.InnerType.Pointer -}}
        if v == nil {
        res = append(res, nil)
        continue
        }
    {{ end -}}
        d := genArg{{ .Slice.InnerType.GetGenFuncName }}({{ if .IsPointer }}*{{ end }}v)
        res = append(res, d)
    }

    return res
    }
{{ end -}}

{{- define "gen_pointer_reverse" }}
    {{- $Package := "wiki_yandex" -}}
    {{- $ProtoPackageName := "proto_client" -}}

    genArg{{ .GetGenFuncName }} := func(data {{ .GetReturnTypeName $ProtoPackageName (toBoolPointer true) false }}) {{ .GetReturnTypeName $Package (toBoolPointer true) true }} {
        if data == nil {
            return nil
        }

        res := genArg{{ .Pointer.InnerType.GetGenFuncName }}({{ if .Pointer.InnerType.IsPointer }}*{{ end }}data)
        return &res
    }
{{ end -}}

{{- if .Function.IsSubscription }}
func {{ .Function.FunctionName }}(argsBytes []byte, callback goapi.EventCallback) (goapi.Subscription, error) {
    args := {{ $ProtoPackageName }}.{{ .Function.FunctionName }}Args{}

    if err := proto.Unmarshal(argsBytes, &args); err != nil {
        return nil, err
    }

    return {{ .Package }}.{{ .Function.FunctionName }}({{ range $item := .Function.Args }}{{ $item.GetUpperCamelCaseName "args." "default" }}, {{ end }}func(data {{ .GetEventTypeName }}) {
        {{ range $i, $item := .FlatFields -}}
            //{{ $i }}
            {{- template "gen_field" $item }}
        {{ end -}}

        bytes, err := proto.Marshal(gen{{ .GetLastFunction.GetGenFuncName }}(data))
        if err == nil {
            callback.OnEvent(bytes)
        }
    })
}

func getEventName{{ .Function.FunctionName }}(argsBytes []byte) (string, error) {
    args := {{ $ProtoPackageName }}.{{ .Function.FunctionName }}Args{}

    if err := proto.Unmarshal(argsBytes, &args); err != nil {
        return "", err
    }

    fullName := "{{ .Function.FunctionName }}" + fmt.Sprintf("
    {{- range $item := $Args }}:%v{{ end -}}
    ", {{- range $i, $item := $Args }}{{ $item.GetUpperCamelCaseName "args." "" -}}
    {{- if .NotIsLastField $Args $i }}, {{ end -}}
    {{- end -}})

    return fullName, nil
}
{{ else if .Function.IsPure }}
// function is pure
func {{ .Function.FunctionName }}(
    {{- range $index, $item := $Args }}
        {{- $item.Name }} {{ .GetReturnTypeName "" (toBoolPointer .Type.IsPointer) false }}
        {{- if .NotIsLastField $Args $index }}, {{ end }}
    {{- end }}) (
    {{- range $index, $item := $ReturnValues }}
        {{- .GetReturnTypeName "" (toBoolPointer .Type.IsPointer) false }}
        {{- if .NotIsLastField $ReturnValues $index }}, {{ end }}
    {{- end -}}
    ) {
    {{ if .Function.HasReturnTypes }}return {{ end }}{{ .Package }}.{{ .Function.FunctionName }}(
    {{- range $index, $item := $Args }}
    {{- $item.Name }}
    {{- if .NotIsLastField $Args $index }}, {{ end -}}
    {{end}})
}
{{ else }}
{{/*{{- $LastArgFunctionName := .GetLastArgFunctionName -}}*/}}
func callAdapterFor{{ .Function.FunctionName }}(argsBytes []byte, callback goapi.FuncCallback) {
    args := {{ $ProtoPackageName }}.{{ .Function.FunctionName }}Args{}

    if err := proto.Unmarshal(argsBytes, &args); err != nil {
        callback.OnError(err.Error())
        return
    }

    {{ range $i, $item := .FlatArgFields -}}
        //{{ $i }}
        {{- template "gen_field_reverse" $item }}
    {{ end }}

    res, err := {{ .Package }}.{{ .Function.FunctionName }}(
    {{- range $i, $item := $Args -}}
        genArg{{ $item.Type.GetGenFuncName }}(args.{{ $item.ToUpperCamelCase }})
    {{- if .NotIsLastField $Args $i }}, {{ end -}}
    {{- end }})
    if err != nil {
        callback.OnError(err.Error())
    } else {
    {{ range $i, $item := .FlatFields -}}
        //{{ $i }}
        {{- template "gen_field" $item }}
    {{ end }}
        bytes, err := proto.Marshal(gen{{ .GetLastFunction.GetGenFuncName }}(res))
        if err == nil {
            callback.OnSuccess(bytes)
        } else {
            callback.OnError(err.Error())
        }
    }
}
{{ end }}
