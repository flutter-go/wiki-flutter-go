{{- $GeneratedPackagePath := .GetDartGeneratedPath -}}
import 'dart:async';
import 'dart:collection';
import 'dart:typed_data';

import 'package:{{ $GeneratedPackagePath }}/wiki_yandex.pb.dart';
import 'package:{{ $GeneratedPackagePath }}/timestamp.pb.dart';
import 'package:{{ $GeneratedPackagePath }}/wrappers.pb.dart';
import 'package:flutter/material.dart' as MaterialDartLib;
import 'package:flutter/services.dart';
import 'package:{{ $GeneratedPackagePath }}/debugwebsocket.pbgrpc.dart';
import 'package:grpc/grpc.dart';
import 'package:protobuf/protobuf.dart';
import 'package:uuid/uuid.dart';

const platform = const MethodChannel("ru.penf00k.wiki_flutter_go.CHANNEL");
const eventChannel = const EventChannel("ru.penf00k.wiki_flutter_go.EVENT_CHANNEL");

GoApiCaller apiCaller = GoApiCaller({{ if .CodeList.Dev }}DebugGoApiCaller(){{ else }}ProdGoApiCaller(){{ end }});

Future<dynamic> handleMethod(MethodCall call) async {
	switch (call.method) {
		case "subscriptionEvent":
			var args = call.arguments as Map<dynamic, dynamic>;
			var fullSubscriptionName = args["fullSubscriptionName"];
			var payload = args["payload"] as Uint8List;

			apiCaller.onEvent(fullSubscriptionName, payload);
			break;
	}
}

typedef SubscriptionCallback<T extends GeneratedMessage> = void Function(T);
typedef SubscriptionBytesCallback = void Function(Uint8List);

abstract class ApiCaller {
  Future<Uint8List> callMethod(String name, GeneratedMessage args);

  Future<SubscriptionData> subscribe<T>(
      String name, GeneratedMessage args, SubscriptionBytesCallback callback);

  Future<bool> unsubscribe(SubscriptionData subscriptionData);

  void onEvent(String fullSubscriptionName, Uint8List payload);
}

class SubscriptionData {
  final String fullSubscriptionName;
  final String id;

  SubscriptionData(this.fullSubscriptionName, this.id);
}

class GoApiCaller extends ApiCaller {
  final ApiCaller apiCaller;

  GoApiCaller(this.apiCaller);

  @override
  Future<Uint8List> callMethod(String name, GeneratedMessage args) {
    return apiCaller.callMethod(name, args);
  }

  @override
  Future<SubscriptionData> subscribe<T>(
      String name, GeneratedMessage args, SubscriptionBytesCallback callback) {
    return apiCaller.subscribe(name, args, callback);
  }

  @override
  Future<bool> unsubscribe(SubscriptionData subscriptionData) {
    return apiCaller.unsubscribe(subscriptionData);
  }

  void onEvent(String fullSubscriptionName, Uint8List payload) {
    return apiCaller.onEvent(fullSubscriptionName, payload);
  }
}

class ProdGoApiCaller extends ApiCaller {
  final idGenerator = Uuid();
  final globalSubscriptions =
      Map<String, Map<String, SubscriptionBytesCallback>>();

  @override
  Future<Uint8List> callMethod(String name, GeneratedMessage args) async {
    var mapped = {
      "methodName": name,
      "args": args.writeToBuffer(),
    };

    return await platform.invokeMethod("callMethod", mapped) as Uint8List;
  }

  @override
  Future<SubscriptionData> subscribe<T>(String name, GeneratedMessage args,
      SubscriptionBytesCallback callback) async {

    var mapped = {
      "subscriptionName": name,
      "args": args.writeToBuffer(),
    };

    final bytes = await platform.invokeMethod("subscribe", mapped);
    final fullSubscriptionName = StringValue.fromBuffer(bytes).value;

    var subs = globalSubscriptions[fullSubscriptionName];
    if (subs == null) {
      subs = Map<String, SubscriptionBytesCallback>();
      globalSubscriptions[fullSubscriptionName] = subs;
    }

    final uniqueId = idGenerator.v4();

    subs[uniqueId] = callback;

    return SubscriptionData(fullSubscriptionName, uniqueId);
  }

  @override
  Future<bool> unsubscribe(SubscriptionData subscriptionData) async {
    final fullSubscriptionName = subscriptionData.fullSubscriptionName;

    var subs = globalSubscriptions[fullSubscriptionName];
    if (subs == null) {
      return true;
    }

    subs.remove(subscriptionData.id);

    if (subs.length == 0) globalSubscriptions.remove(fullSubscriptionName);
    var mapped = {
      "fullSubscriptionName": fullSubscriptionName,
    };

    await platform.invokeMethod("unsubscribe", mapped);
    return true;
  }

  @override
  void onEvent(String fullSubscriptionName, Uint8List payload) {
    var subs = globalSubscriptions[fullSubscriptionName];
    if (subs == null) {
      return;
    }

    subs.forEach((id, sub) {
      final callback = subs[id];
      if (callback != null) callback(payload);
    });
  }
}

class SubscribeData {
  final SubscribeArgs args;
  final SubscriptionBytesCallback callback;

  SubscribeData(this.args, this.callback);
}

class DebugGoApiCaller extends ApiCaller {
  final reconnectTimeout = Duration(seconds: 1);
  final idGenerator = Uuid();
  final globalSubscriptions =
      Map<String, Map<String, SubscriptionBytesCallback>>();
  final pendingCalls = Queue<CallMethodArgs>();
  final pendingSubs = Queue<SubscribeData>();

  ClientChannel channel;
  DebugClient client;

  DebugGoApiCaller() {
    _connect();
  }

  void _connect() {
    channel = ClientChannel('{{ .CodeList.ServerIp }}',
        port: {{ .CodeList.Port }},
        options: const ChannelOptions(
          credentials: const ChannelCredentials.insecure(),
          idleTimeout: Duration(days: 10),
        ));
    client = DebugClient(channel,
        options: CallOptions(
          timeout: Duration(days: 10),
        ));


    final eventStream = client.registerEventCallback(Empty());
    eventStream.listen((event) {
      try {
        onEvent(event.fullSubscriptionName, Uint8List.fromList(event.data));
      } catch(e) {
        MaterialDartLib.debugPrint("error sending event: $e");
      }
    }, onError: (e) {
      MaterialDartLib.debugPrint(
          "error on event stream: $e; Restarting in $reconnectTimeout...");
      Timer(reconnectTimeout, () {
        _connect();
      });
    });

    _sendPending();

  }

  void _sendPending() {
    while (pendingCalls.isNotEmpty) {
      final callData = pendingCalls.first;
      _callMethod(callData);

      pendingCalls.removeFirst();
    }
    while (pendingSubs.isNotEmpty) {
      final subData = pendingSubs.first;
      _subscribe(subData.args, subData.callback);

      pendingSubs.removeFirst();
    }
  }

  @override
  Future<Uint8List> callMethod(String name, GeneratedMessage args) async {
    final data = CallMethodArgs()
      ..methodName = name
      ..args = args.writeToBuffer();

    return _callMethod(data);
  }

  Future<Uint8List> _callMethod(CallMethodArgs data) async {
    try {
      final result = await client.callMethod(data);
      return Uint8List.fromList(result.value);
    } on GrpcError catch (e) {
      if (e.code != StatusCode.unknown) {
        pendingCalls.add(data);
      }
      rethrow;
    }
  }

  @override
  Future<SubscriptionData> subscribe<T>(String name, GeneratedMessage args,
      SubscriptionBytesCallback callback) async {
    final data = SubscribeArgs()
      ..subscriptionName = name
      ..args = args.writeToBuffer();

    return _subscribe(data, callback);
  }

  Future<SubscriptionData> _subscribe<T>(
      SubscribeArgs data, SubscriptionBytesCallback callback) async {
    try {
      final unsubscribeArgs = await client.subscribe(data);
      final fullSubscriptionName = unsubscribeArgs.fullSubscriptionName;

      var subs = globalSubscriptions[fullSubscriptionName];
      if (subs == null) {
        subs = Map<String, SubscriptionBytesCallback>();
        globalSubscriptions[fullSubscriptionName] = subs;
      }

      final uniqueId = idGenerator.v4();

      subs[uniqueId] = callback;

      return SubscriptionData(fullSubscriptionName, uniqueId);
    } on GrpcError catch (e) {
      if (e.code != StatusCode.unknown) {
        final subData = SubscribeData(data, callback);
        pendingSubs.add(subData);
      }
      rethrow;
    }
  }

  @override
  Future<bool> unsubscribe(SubscriptionData subscriptionData) async {
    final fullSubscriptionName = subscriptionData.fullSubscriptionName;

    var subs = globalSubscriptions[fullSubscriptionName];
    if (subs == null) {
      return true;
    }

    subs.remove(subscriptionData.id);
    if (subs.length == 0) globalSubscriptions.remove(fullSubscriptionName);

    final args = UnsubscribeArgs()..fullSubscriptionName = fullSubscriptionName;
    await client.unsubscribe(args);
    return true;
  }

  @override
  void onEvent(String fullSubscriptionName, Uint8List payload) {
    var subs = globalSubscriptions[fullSubscriptionName];
    if (subs == null) {
      return;
    }

    subs.forEach((id, sub) {
      final callback = subs[id];
      if (callback != null) callback(payload);
    });
  }
}
