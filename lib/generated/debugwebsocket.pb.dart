///
//  Generated code. Do not modify.
//  source: debugwebsocket.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' show int, bool, double, String, List, Map, override;

import 'package:protobuf/protobuf.dart' as $pb;

class CallMethodArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('CallMethodArgs', package: const $pb.PackageName('debugwebsocket'))
    ..aOS(1, 'methodName')
    ..a<List<int>>(2, 'args', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  CallMethodArgs() : super();
  CallMethodArgs.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  CallMethodArgs.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  CallMethodArgs clone() => new CallMethodArgs()..mergeFromMessage(this);
  CallMethodArgs copyWith(void Function(CallMethodArgs) updates) => super.copyWith((message) => updates(message as CallMethodArgs));
  $pb.BuilderInfo get info_ => _i;
  static CallMethodArgs create() => new CallMethodArgs();
  CallMethodArgs createEmptyInstance() => create();
  static $pb.PbList<CallMethodArgs> createRepeated() => new $pb.PbList<CallMethodArgs>();
  static CallMethodArgs getDefault() => _defaultInstance ??= create()..freeze();
  static CallMethodArgs _defaultInstance;
  static void $checkItem(CallMethodArgs v) {
    if (v is! CallMethodArgs) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get methodName => $_getS(0, '');
  set methodName(String v) { $_setString(0, v); }
  bool hasMethodName() => $_has(0);
  void clearMethodName() => clearField(1);

  List<int> get args => $_getN(1);
  set args(List<int> v) { $_setBytes(1, v); }
  bool hasArgs() => $_has(1);
  void clearArgs() => clearField(2);
}

class SubscribeArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SubscribeArgs', package: const $pb.PackageName('debugwebsocket'))
    ..aOS(1, 'subscriptionName')
    ..a<List<int>>(2, 'args', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  SubscribeArgs() : super();
  SubscribeArgs.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SubscribeArgs.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SubscribeArgs clone() => new SubscribeArgs()..mergeFromMessage(this);
  SubscribeArgs copyWith(void Function(SubscribeArgs) updates) => super.copyWith((message) => updates(message as SubscribeArgs));
  $pb.BuilderInfo get info_ => _i;
  static SubscribeArgs create() => new SubscribeArgs();
  SubscribeArgs createEmptyInstance() => create();
  static $pb.PbList<SubscribeArgs> createRepeated() => new $pb.PbList<SubscribeArgs>();
  static SubscribeArgs getDefault() => _defaultInstance ??= create()..freeze();
  static SubscribeArgs _defaultInstance;
  static void $checkItem(SubscribeArgs v) {
    if (v is! SubscribeArgs) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get subscriptionName => $_getS(0, '');
  set subscriptionName(String v) { $_setString(0, v); }
  bool hasSubscriptionName() => $_has(0);
  void clearSubscriptionName() => clearField(1);

  List<int> get args => $_getN(1);
  set args(List<int> v) { $_setBytes(1, v); }
  bool hasArgs() => $_has(1);
  void clearArgs() => clearField(2);
}

class UnsubscribeArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('UnsubscribeArgs', package: const $pb.PackageName('debugwebsocket'))
    ..aOS(1, 'fullSubscriptionName')
    ..hasRequiredFields = false
  ;

  UnsubscribeArgs() : super();
  UnsubscribeArgs.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  UnsubscribeArgs.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  UnsubscribeArgs clone() => new UnsubscribeArgs()..mergeFromMessage(this);
  UnsubscribeArgs copyWith(void Function(UnsubscribeArgs) updates) => super.copyWith((message) => updates(message as UnsubscribeArgs));
  $pb.BuilderInfo get info_ => _i;
  static UnsubscribeArgs create() => new UnsubscribeArgs();
  UnsubscribeArgs createEmptyInstance() => create();
  static $pb.PbList<UnsubscribeArgs> createRepeated() => new $pb.PbList<UnsubscribeArgs>();
  static UnsubscribeArgs getDefault() => _defaultInstance ??= create()..freeze();
  static UnsubscribeArgs _defaultInstance;
  static void $checkItem(UnsubscribeArgs v) {
    if (v is! UnsubscribeArgs) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get fullSubscriptionName => $_getS(0, '');
  set fullSubscriptionName(String v) { $_setString(0, v); }
  bool hasFullSubscriptionName() => $_has(0);
  void clearFullSubscriptionName() => clearField(1);
}

class Payload extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Payload', package: const $pb.PackageName('debugwebsocket'))
    ..a<List<int>>(1, 'value', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  Payload() : super();
  Payload.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Payload.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Payload clone() => new Payload()..mergeFromMessage(this);
  Payload copyWith(void Function(Payload) updates) => super.copyWith((message) => updates(message as Payload));
  $pb.BuilderInfo get info_ => _i;
  static Payload create() => new Payload();
  Payload createEmptyInstance() => create();
  static $pb.PbList<Payload> createRepeated() => new $pb.PbList<Payload>();
  static Payload getDefault() => _defaultInstance ??= create()..freeze();
  static Payload _defaultInstance;
  static void $checkItem(Payload v) {
    if (v is! Payload) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  List<int> get value => $_getN(0);
  set value(List<int> v) { $_setBytes(0, v); }
  bool hasValue() => $_has(0);
  void clearValue() => clearField(1);
}

class SubscriptionEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SubscriptionEvent', package: const $pb.PackageName('debugwebsocket'))
    ..aOS(1, 'fullSubscriptionName')
    ..a<List<int>>(2, 'data', $pb.PbFieldType.OY)
    ..hasRequiredFields = false
  ;

  SubscriptionEvent() : super();
  SubscriptionEvent.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SubscriptionEvent.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SubscriptionEvent clone() => new SubscriptionEvent()..mergeFromMessage(this);
  SubscriptionEvent copyWith(void Function(SubscriptionEvent) updates) => super.copyWith((message) => updates(message as SubscriptionEvent));
  $pb.BuilderInfo get info_ => _i;
  static SubscriptionEvent create() => new SubscriptionEvent();
  SubscriptionEvent createEmptyInstance() => create();
  static $pb.PbList<SubscriptionEvent> createRepeated() => new $pb.PbList<SubscriptionEvent>();
  static SubscriptionEvent getDefault() => _defaultInstance ??= create()..freeze();
  static SubscriptionEvent _defaultInstance;
  static void $checkItem(SubscriptionEvent v) {
    if (v is! SubscriptionEvent) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get fullSubscriptionName => $_getS(0, '');
  set fullSubscriptionName(String v) { $_setString(0, v); }
  bool hasFullSubscriptionName() => $_has(0);
  void clearFullSubscriptionName() => clearField(1);

  List<int> get data => $_getN(1);
  set data(List<int> v) { $_setBytes(1, v); }
  bool hasData() => $_has(1);
  void clearData() => clearField(2);
}

class SubscribeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('SubscribeResponse', package: const $pb.PackageName('debugwebsocket'))
    ..aOS(1, 'fullSubscriptionName')
    ..hasRequiredFields = false
  ;

  SubscribeResponse() : super();
  SubscribeResponse.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SubscribeResponse.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SubscribeResponse clone() => new SubscribeResponse()..mergeFromMessage(this);
  SubscribeResponse copyWith(void Function(SubscribeResponse) updates) => super.copyWith((message) => updates(message as SubscribeResponse));
  $pb.BuilderInfo get info_ => _i;
  static SubscribeResponse create() => new SubscribeResponse();
  SubscribeResponse createEmptyInstance() => create();
  static $pb.PbList<SubscribeResponse> createRepeated() => new $pb.PbList<SubscribeResponse>();
  static SubscribeResponse getDefault() => _defaultInstance ??= create()..freeze();
  static SubscribeResponse _defaultInstance;
  static void $checkItem(SubscribeResponse v) {
    if (v is! SubscribeResponse) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }

  String get fullSubscriptionName => $_getS(0, '');
  set fullSubscriptionName(String v) { $_setString(0, v); }
  bool hasFullSubscriptionName() => $_has(0);
  void clearFullSubscriptionName() => clearField(1);
}

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Empty', package: const $pb.PackageName('debugwebsocket'))
    ..hasRequiredFields = false
  ;

  Empty() : super();
  Empty.fromBuffer(List<int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Empty.fromJson(String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Empty clone() => new Empty()..mergeFromMessage(this);
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty));
  $pb.BuilderInfo get info_ => _i;
  static Empty create() => new Empty();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => new $pb.PbList<Empty>();
  static Empty getDefault() => _defaultInstance ??= create()..freeze();
  static Empty _defaultInstance;
  static void $checkItem(Empty v) {
    if (v is! Empty) $pb.checkItemFailed(v, _i.qualifiedMessageName);
  }
}

