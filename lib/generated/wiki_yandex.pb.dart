///
//  Generated code. Do not modify.
//  source: wiki_yandex.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import

// ignore: UNUSED_SHOWN_NAME
import 'dart:core' as $core show int, bool, double, String, List, Map, override;

import 'package:protobuf/protobuf.dart' as $pb;

class InitializeArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('InitializeArgs', package: const $pb.PackageName('proto_client'))
    ..aOS(1, 'host')
    ..hasRequiredFields = false
  ;

  InitializeArgs() : super();
  InitializeArgs.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  InitializeArgs.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  InitializeArgs clone() => new InitializeArgs()..mergeFromMessage(this);
  InitializeArgs copyWith(void Function(InitializeArgs) updates) => super.copyWith((message) => updates(message as InitializeArgs));
  $pb.BuilderInfo get info_ => _i;
  static InitializeArgs create() => new InitializeArgs();
  InitializeArgs createEmptyInstance() => create();
  static $pb.PbList<InitializeArgs> createRepeated() => new $pb.PbList<InitializeArgs>();
  static InitializeArgs getDefault() => _defaultInstance ??= create()..freeze();
  static InitializeArgs _defaultInstance;

  $core.String get host => $_getS(0, '');
  set host($core.String v) { $_setString(0, v); }
  $core.bool hasHost() => $_has(0);
  void clearHost() => clearField(1);
}

class JustForFunArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('JustForFunArgs', package: const $pb.PackageName('proto_client'))
    ..aOS(1, 'text')
    ..hasRequiredFields = false
  ;

  JustForFunArgs() : super();
  JustForFunArgs.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  JustForFunArgs.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  JustForFunArgs clone() => new JustForFunArgs()..mergeFromMessage(this);
  JustForFunArgs copyWith(void Function(JustForFunArgs) updates) => super.copyWith((message) => updates(message as JustForFunArgs));
  $pb.BuilderInfo get info_ => _i;
  static JustForFunArgs create() => new JustForFunArgs();
  JustForFunArgs createEmptyInstance() => create();
  static $pb.PbList<JustForFunArgs> createRepeated() => new $pb.PbList<JustForFunArgs>();
  static JustForFunArgs getDefault() => _defaultInstance ??= create()..freeze();
  static JustForFunArgs _defaultInstance;

  $core.String get text => $_getS(0, '');
  set text($core.String v) { $_setString(0, v); }
  $core.bool hasText() => $_has(0);
  void clearText() => clearField(1);
}

class GetAllArticlesArgs extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('GetAllArticlesArgs', package: const $pb.PackageName('proto_client'))
    ..a<$core.double>(1, 'lat', $pb.PbFieldType.OF)
    ..a<$core.double>(2, 'lng', $pb.PbFieldType.OF)
    ..hasRequiredFields = false
  ;

  GetAllArticlesArgs() : super();
  GetAllArticlesArgs.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  GetAllArticlesArgs.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  GetAllArticlesArgs clone() => new GetAllArticlesArgs()..mergeFromMessage(this);
  GetAllArticlesArgs copyWith(void Function(GetAllArticlesArgs) updates) => super.copyWith((message) => updates(message as GetAllArticlesArgs));
  $pb.BuilderInfo get info_ => _i;
  static GetAllArticlesArgs create() => new GetAllArticlesArgs();
  GetAllArticlesArgs createEmptyInstance() => create();
  static $pb.PbList<GetAllArticlesArgs> createRepeated() => new $pb.PbList<GetAllArticlesArgs>();
  static GetAllArticlesArgs getDefault() => _defaultInstance ??= create()..freeze();
  static GetAllArticlesArgs _defaultInstance;

  $core.double get lat => $_getN(0);
  set lat($core.double v) { $_setFloat(0, v); }
  $core.bool hasLat() => $_has(0);
  void clearLat() => clearField(1);

  $core.double get lng => $_getN(1);
  set lng($core.double v) { $_setFloat(1, v); }
  $core.bool hasLng() => $_has(1);
  void clearLng() => clearField(2);
}

class LovelyObject extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('LovelyObject', package: const $pb.PackageName('proto_client'))
    ..aOS(1, 'textInsideYa')
    ..hasRequiredFields = false
  ;

  LovelyObject() : super();
  LovelyObject.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  LovelyObject.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  LovelyObject clone() => new LovelyObject()..mergeFromMessage(this);
  LovelyObject copyWith(void Function(LovelyObject) updates) => super.copyWith((message) => updates(message as LovelyObject));
  $pb.BuilderInfo get info_ => _i;
  static LovelyObject create() => new LovelyObject();
  LovelyObject createEmptyInstance() => create();
  static $pb.PbList<LovelyObject> createRepeated() => new $pb.PbList<LovelyObject>();
  static LovelyObject getDefault() => _defaultInstance ??= create()..freeze();
  static LovelyObject _defaultInstance;

  $core.String get textInsideYa => $_getS(0, '');
  set textInsideYa($core.String v) { $_setString(0, v); }
  $core.bool hasTextInsideYa() => $_has(0);
  void clearTextInsideYa() => clearField(1);
}

class ArticleList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('ArticleList', package: const $pb.PackageName('proto_client'))
    ..pc<Article>(1, 'list', $pb.PbFieldType.PM,Article.create)
    ..hasRequiredFields = false
  ;

  ArticleList() : super();
  ArticleList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  ArticleList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  ArticleList clone() => new ArticleList()..mergeFromMessage(this);
  ArticleList copyWith(void Function(ArticleList) updates) => super.copyWith((message) => updates(message as ArticleList));
  $pb.BuilderInfo get info_ => _i;
  static ArticleList create() => new ArticleList();
  ArticleList createEmptyInstance() => create();
  static $pb.PbList<ArticleList> createRepeated() => new $pb.PbList<ArticleList>();
  static ArticleList getDefault() => _defaultInstance ??= create()..freeze();
  static ArticleList _defaultInstance;

  $core.List<Article> get list => $_getList(0);
}

class Article extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Article', package: const $pb.PackageName('proto_client'))
    ..a<$core.int>(1, 'pageID', $pb.PbFieldType.O3)
    ..aOS(2, 'title')
    ..pc<Image>(3, 'images', $pb.PbFieldType.PM,Image.create)
    ..hasRequiredFields = false
  ;

  Article() : super();
  Article.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Article.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Article clone() => new Article()..mergeFromMessage(this);
  Article copyWith(void Function(Article) updates) => super.copyWith((message) => updates(message as Article));
  $pb.BuilderInfo get info_ => _i;
  static Article create() => new Article();
  Article createEmptyInstance() => create();
  static $pb.PbList<Article> createRepeated() => new $pb.PbList<Article>();
  static Article getDefault() => _defaultInstance ??= create()..freeze();
  static Article _defaultInstance;

  $core.int get pageID => $_get(0, 0);
  set pageID($core.int v) { $_setSignedInt32(0, v); }
  $core.bool hasPageID() => $_has(0);
  void clearPageID() => clearField(1);

  $core.String get title => $_getS(1, '');
  set title($core.String v) { $_setString(1, v); }
  $core.bool hasTitle() => $_has(1);
  void clearTitle() => clearField(2);

  $core.List<Image> get images => $_getList(2);
}

class Image extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = new $pb.BuilderInfo('Image', package: const $pb.PackageName('proto_client'))
    ..aOS(1, 'title')
    ..aOS(2, 'url')
    ..hasRequiredFields = false
  ;

  Image() : super();
  Image.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Image.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Image clone() => new Image()..mergeFromMessage(this);
  Image copyWith(void Function(Image) updates) => super.copyWith((message) => updates(message as Image));
  $pb.BuilderInfo get info_ => _i;
  static Image create() => new Image();
  Image createEmptyInstance() => create();
  static $pb.PbList<Image> createRepeated() => new $pb.PbList<Image>();
  static Image getDefault() => _defaultInstance ??= create()..freeze();
  static Image _defaultInstance;

  $core.String get title => $_getS(0, '');
  set title($core.String v) { $_setString(0, v); }
  $core.bool hasTitle() => $_has(0);
  void clearTitle() => clearField(1);

  $core.String get url => $_getS(1, '');
  set url($core.String v) { $_setString(1, v); }
  $core.bool hasUrl() => $_has(1);
  void clearUrl() => clearField(2);
}

