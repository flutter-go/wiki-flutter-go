///
//  Generated code. Do not modify.
//  source: wiki_yandex.proto
///
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import

const InitializeArgs$json = const {
  '1': 'InitializeArgs',
  '2': const [
    const {'1': 'host', '3': 1, '4': 1, '5': 9, '10': 'host'},
  ],
};

const JustForFunArgs$json = const {
  '1': 'JustForFunArgs',
  '2': const [
    const {'1': 'text', '3': 1, '4': 1, '5': 9, '10': 'text'},
  ],
};

const GetAllArticlesArgs$json = const {
  '1': 'GetAllArticlesArgs',
  '2': const [
    const {'1': 'lat', '3': 1, '4': 1, '5': 2, '10': 'lat'},
    const {'1': 'lng', '3': 2, '4': 1, '5': 2, '10': 'lng'},
  ],
};

const LovelyObject$json = const {
  '1': 'LovelyObject',
  '2': const [
    const {'1': 'TextInsideYa', '3': 1, '4': 1, '5': 9, '10': 'TextInsideYa'},
  ],
};

const ArticleList$json = const {
  '1': 'ArticleList',
  '2': const [
    const {'1': 'List', '3': 1, '4': 3, '5': 11, '6': '.proto_client.Article', '10': 'List'},
  ],
};

const Article$json = const {
  '1': 'Article',
  '2': const [
    const {'1': 'PageID', '3': 1, '4': 1, '5': 5, '10': 'PageID'},
    const {'1': 'Title', '3': 2, '4': 1, '5': 9, '10': 'Title'},
    const {'1': 'Images', '3': 3, '4': 3, '5': 11, '6': '.proto_client.Image', '10': 'Images'},
  ],
};

const Image$json = const {
  '1': 'Image',
  '2': const [
    const {'1': 'Title', '3': 1, '4': 1, '5': 9, '10': 'Title'},
    const {'1': 'Url', '3': 2, '4': 1, '5': 9, '10': 'Url'},
  ],
};

