///
//  Generated code. Do not modify.
//  source: debugwebsocket.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

const CallMethodArgs$json = const {
  '1': 'CallMethodArgs',
  '2': const [
    const {'1': 'MethodName', '3': 1, '4': 1, '5': 9, '10': 'MethodName'},
    const {'1': 'Args', '3': 2, '4': 1, '5': 12, '10': 'Args'},
  ],
};

const SubscribeArgs$json = const {
  '1': 'SubscribeArgs',
  '2': const [
    const {'1': 'SubscriptionName', '3': 1, '4': 1, '5': 9, '10': 'SubscriptionName'},
    const {'1': 'Args', '3': 2, '4': 1, '5': 12, '10': 'Args'},
  ],
};

const UnsubscribeArgs$json = const {
  '1': 'UnsubscribeArgs',
  '2': const [
    const {'1': 'FullSubscriptionName', '3': 1, '4': 1, '5': 9, '10': 'FullSubscriptionName'},
  ],
};

const Payload$json = const {
  '1': 'Payload',
  '2': const [
    const {'1': 'Value', '3': 1, '4': 1, '5': 12, '10': 'Value'},
  ],
};

const SubscriptionEvent$json = const {
  '1': 'SubscriptionEvent',
  '2': const [
    const {'1': 'FullSubscriptionName', '3': 1, '4': 1, '5': 9, '10': 'FullSubscriptionName'},
    const {'1': 'Data', '3': 2, '4': 1, '5': 12, '10': 'Data'},
  ],
};

const SubscribeResponse$json = const {
  '1': 'SubscribeResponse',
  '2': const [
    const {'1': 'FullSubscriptionName', '3': 1, '4': 1, '5': 9, '10': 'FullSubscriptionName'},
  ],
};

const Empty$json = const {
  '1': 'Empty',
};

