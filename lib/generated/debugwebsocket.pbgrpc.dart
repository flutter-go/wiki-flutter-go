///
//  Generated code. Do not modify.
//  source: debugwebsocket.proto
///
// ignore_for_file: non_constant_identifier_names,library_prefixes,unused_import

import 'dart:async' as $async;

import 'package:grpc/grpc.dart' as $grpc;
import 'debugwebsocket.pb.dart';
export 'debugwebsocket.pb.dart';

class DebugClient extends $grpc.Client {
  static final _$callMethod = new $grpc.ClientMethod<CallMethodArgs, Payload>(
      '/debugwebsocket.Debug/CallMethod',
      (CallMethodArgs value) => value.writeToBuffer(),
      (List<int> value) => new Payload.fromBuffer(value));
  static final _$subscribe =
      new $grpc.ClientMethod<SubscribeArgs, UnsubscribeArgs>(
          '/debugwebsocket.Debug/Subscribe',
          (SubscribeArgs value) => value.writeToBuffer(),
          (List<int> value) => new UnsubscribeArgs.fromBuffer(value));
  static final _$unsubscribe = new $grpc.ClientMethod<UnsubscribeArgs, Empty>(
      '/debugwebsocket.Debug/Unsubscribe',
      (UnsubscribeArgs value) => value.writeToBuffer(),
      (List<int> value) => new Empty.fromBuffer(value));
  static final _$registerEventCallback =
      new $grpc.ClientMethod<Empty, SubscriptionEvent>(
          '/debugwebsocket.Debug/RegisterEventCallback',
          (Empty value) => value.writeToBuffer(),
          (List<int> value) => new SubscriptionEvent.fromBuffer(value));

  DebugClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<Payload> callMethod(CallMethodArgs request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$callMethod, new $async.Stream.fromIterable([request]),
        options: options);
    return new $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<UnsubscribeArgs> subscribe(SubscribeArgs request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$subscribe, new $async.Stream.fromIterable([request]),
        options: options);
    return new $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<Empty> unsubscribe(UnsubscribeArgs request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$unsubscribe, new $async.Stream.fromIterable([request]),
        options: options);
    return new $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<SubscriptionEvent> registerEventCallback(Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$registerEventCallback, new $async.Stream.fromIterable([request]),
        options: options);
    return new $grpc.ResponseStream(call);
  }
}

abstract class DebugServiceBase extends $grpc.Service {
  String get $name => 'debugwebsocket.Debug';

  DebugServiceBase() {
    $addMethod(new $grpc.ServiceMethod<CallMethodArgs, Payload>(
        'CallMethod',
        callMethod_Pre,
        false,
        false,
        (List<int> value) => new CallMethodArgs.fromBuffer(value),
        (Payload value) => value.writeToBuffer()));
    $addMethod(new $grpc.ServiceMethod<SubscribeArgs, UnsubscribeArgs>(
        'Subscribe',
        subscribe_Pre,
        false,
        false,
        (List<int> value) => new SubscribeArgs.fromBuffer(value),
        (UnsubscribeArgs value) => value.writeToBuffer()));
    $addMethod(new $grpc.ServiceMethod<UnsubscribeArgs, Empty>(
        'Unsubscribe',
        unsubscribe_Pre,
        false,
        false,
        (List<int> value) => new UnsubscribeArgs.fromBuffer(value),
        (Empty value) => value.writeToBuffer()));
    $addMethod(new $grpc.ServiceMethod<Empty, SubscriptionEvent>(
        'RegisterEventCallback',
        registerEventCallback_Pre,
        false,
        true,
        (List<int> value) => new Empty.fromBuffer(value),
        (SubscriptionEvent value) => value.writeToBuffer()));
  }

  $async.Future<Payload> callMethod_Pre(
      $grpc.ServiceCall call, $async.Future request) async {
    return callMethod(call, await request);
  }

  $async.Future<UnsubscribeArgs> subscribe_Pre(
      $grpc.ServiceCall call, $async.Future request) async {
    return subscribe(call, await request);
  }

  $async.Future<Empty> unsubscribe_Pre(
      $grpc.ServiceCall call, $async.Future request) async {
    return unsubscribe(call, await request);
  }

  $async.Stream<SubscriptionEvent> registerEventCallback_Pre(
      $grpc.ServiceCall call, $async.Future request) async* {
    yield* registerEventCallback(call, (await request) as Empty);
  }

  $async.Future<Payload> callMethod(
      $grpc.ServiceCall call, CallMethodArgs request);
  $async.Future<UnsubscribeArgs> subscribe(
      $grpc.ServiceCall call, SubscribeArgs request);
  $async.Future<Empty> unsubscribe(
      $grpc.ServiceCall call, UnsubscribeArgs request);
  $async.Stream<SubscriptionEvent> registerEventCallback(
      $grpc.ServiceCall call, Empty request);
}
