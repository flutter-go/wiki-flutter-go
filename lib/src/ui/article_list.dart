import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wiki_flutter_go/generated/wiki_yandex.dart';
import 'package:wiki_flutter_go/generated/wiki_yandex.pb.dart';

class ArticleListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wiki go flutter"),
      ),
      body: SafeArea(
        child: Center(
          child: FutureBuilder<ArticleList>(
            future: getAllArticles(55.734295, 37.613536),
            builder: (BuildContext context, AsyncSnapshot<ArticleList> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('Press button to start.');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return CircularProgressIndicator();
                case ConnectionState.done:
                  if (snapshot.hasError) return Text('Error: ${snapshot.error}');

                  final articles = snapshot.data?.list ?? [];

                  return ListView.builder(
                    itemCount: articles.length,
                    itemBuilder: (BuildContext context, int index) => _buildListItem(context, articles[index]),
                  );
              }
            },
          ),
        ),
      ),
    );
  }

  Widget _buildListItem(BuildContext context, Article article) {
    return ListTile(
      onTap: () {},
      title: Text(article?.title ?? "no name bro"),
      subtitle: Text("Image count: ${article?.images?.length ?? 0}"),
    );
  }
}
